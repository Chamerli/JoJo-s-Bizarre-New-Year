import datetime


format = "%d : %m : %H : %M : %S"

now = datetime.datetime.now()


NY = datetime.datetime((now.year + 1), month=1, day=1)
BTD_OP = datetime.datetime(now.year, month=9, day=30, hour=23, minute=58, second=16)

def delta_time():
    now = datetime.datetime.now().strftime(format)
    return str(now).split(".")[0]

def remain_time():
    now = datetime.datetime.now()

    time_until_ny = NY - now
    
    return str(time_until_ny).split(".")[0]